mport React from "react";
import ReactDOM from "react-dom";

import "./styles.css";
const goomap = {
    plot: '<iframe src="https://codesandbox.io/embed/q7jmjyplvq?fontsize=14" title="Plotly All Graph Types" allow="geolocation; microphone; camera; midi; vr; accelerometer; gyroscope; payment; ambient-light-sensor; encrypted-media" style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;" sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"></iframe>'
};

function Iframe(props) {
    return ( <
        div dangerouslySetInnerHTML = {
            { __html: props.iframe ? props.iframe : "" }
        }
        />
    );
}

function App() {
    return ( <
        div className = "App" >
        <
        h1 > I frame Demo < /h1> <
        Iframe iframe = { demos["plotly"] }
        />, < /
        div >
    );
}

const rootElement = document.getElementById("root");
ReactDOM.render( < App / > , rootElement);
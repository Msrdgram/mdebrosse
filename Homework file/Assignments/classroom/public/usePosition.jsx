import { usePosition } from 'use-position';
//const { latitude, longitude, timestamp, accuracy, error } = usePosition();
const { latitude, longitude, timestamp, accuracy, error } = usePosition(true, {enableHighAccuracy: true});
import React from 'react';
import { usePosition } from '../src/usePosition';
 
export const Demo = () => {
  const { latitude, longitude, timestamp, accuracy, error } = usePosition(true);
 
  return (
    <code>
      latitude: {latitude}<br/>
      longitude: {longitude}<br/>
      timestamp: {timestamp}<br/>
      accuracy: {accuracy && `${accuracy}m`}<br/>
      error: {error}
    </code>
  );
};